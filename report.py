#!/usr/bin/python

def perr(path, descr, line):
    if line >= 0:
        #print("\033[3m{0}\033[m in `{1}' on line {2}".format(descr, path, line))
        print("{0} in `{1}' on line {2}".format(descr, path, line))
    else:
        #print("\033[3m{0}\033[m in `{1}'".format(descr, path))
        print("{0} in `{1}'".format(descr, path))

