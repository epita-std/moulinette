#!/usr/bin/python

from report import perr
import re

def checkFile(path, opt):
    num = 0
    with open(path, "r") as fp:
        content = fp.read()
        
        if len(content) < 1 or content[len(content)-1] != '\n':
            perr(path, "No empty line at the end of file", -1)

        if 'authors-prefix' in opt:
            prefix = opt['authors-prefix'].replace("*", "\*")
        else:
            prefix = "\*"

        lines = content.split('\n')
        for line in lines:
            num += 1
            if len(line) <= 0 and num != len(lines):
                perr(path, "Empty line", num)
            if re.match("^(.{" + "{0}".format(len(prefix.replace("\\", ""))) + "} [a-z][a-z-]{,5}_[a-z0-9])?$", line) is None:
                perr(path, "Bad author format", num)
            elif re.match("^(" + prefix + " [a-z][a-z-]{,5}_[a-z0-9])?$", line) is None:
                perr(path, "Bad line prefix (expected: {0})".format(prefix.replace("\\", "")), num)
