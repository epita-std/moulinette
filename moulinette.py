#!/usr/bin/python
import signal
import sys
import os
import re
from report import perr

def printError(file, type, line = -1):
    perr(file, type, line)

def checkCComment(path, line, num):
    txt = line.strip()
    if re.match("\*/", txt) is not None:
        if re.match("\*/$", txt) is None:
            printError(path, "Last big comment lines aren't empty", num)
        return 0
    elif re.match("\*/", txt) is not None and \
            re.match("/\*", txt) is None:
        printError(path, "\*/ ", num)
    elif re.match("^\*\* ", txt) is None:
        printError(path, "Bad intermediary comment lines", num)
    return 1
        
def checkCLang(path, line, num):
    txt = line.strip()
    if re.match("^#define", txt) is not None and \
            re.match("^#define +[A-Z0-9_]+[ (]?", txt) is None:
        printError(path, "Constant or macro without full uppercase", num)
    elif re.match("^ +#", line) is not None:
        printError(path, "The preprocessor directive mark not on the first column", num)
    elif re.match(".*sizeof[^ ].*", txt) is not None:
        printError(path, "Forgotten space after keyword", num)
    elif re.match("^# *(else|endif)", txt) is not None and \
        re.match("^# *(else|endif) +/", txt) is None:
        printError(path, "Preprocesor condition without comment describing the corresponding condition", num)
    elif re.match("^(if|else|elseif|for|while|do|typedef|struct|return|sizeof)", txt) is not None and \
        re.match("^(if|else|elseif|for|while|do|typedef|struct|return|sizeof)(uble| |$)", txt) is None:
        printError(path, "Forgotten space after keyword", num)
    elif re.match("^(if|else|elseif|for|while|do|typedef|struct)", txt) is not None and \
        re.match("^(if|else|elseif|for|while|do|typedef|struct).*\{", txt) is not None:
        printError(path, "{ on the same line of instruction", num)
    elif re.match("(^/\*|\*/$)", txt) is not None and \
        re.match("^(/\*\*?|\*/)$", txt) is None:
        printError(path, "First and last big comment lines aren't empty", num)
    elif re.match("/\*$", txt) is not None:
        return 1
    elif re.match("^for", txt) is not None and \
        re.match("^for \(.*; .*; .*\)", txt) is None:
        printError(path, "Bad syntax for a for", num)
    elif re.match("^return", txt) is not None and \
        re.match("^return \(.*\)", txt) is None:
        printError(path, "Bad syntax for return", num)
    elif re.match(".*[^=]=.*;", txt) is not None and \
        re.match(".* [><]?[-&%+^*/!><|]?= .*", txt) is None:
        printError(path, "Bad syntax for an affectation", num)
    elif re.match("\r$", txt) is not None:
        printError(path, "DOS CR+LF line terminator detected", num)
    return 0

def checkHFile(path):
    checkCFile(path)

    num = 0
    with open(path, "r") as fp:
        lines = fp.read().split('\n')
#Search for Header protection
        for line in lines:
            num += 1
            line = line.replace("	", "        ")



def checkCFile(path):
    num = 0
    with open(path, "r") as fp:
        lines = fp.read().split('\n')
        typeBlock = 0
        for line in lines:
            num += 1
            line = line.replace("	", "        ")

            if typeBlock == 1:
                typeBlock = checkCComment(path, line, num)
            else:
                typeBlock = checkCLang(path, line, num)

if __name__ == "__main__":
    i = 1
    while i < len(sys.argv):
        checkCFile(sys.argv[i])
        i += 1

