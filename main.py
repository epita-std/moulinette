#!/usr/bin/python

import sys
import os
from report import perr
import c
import authors
import common
import moulinette

def checkDir(path, opt):
    dirList = os.listdir(path)
    filesList = []
    for f in dirList:
        basename = os.path.basename(f)
        if os.path.isdir(f):
            if basename == ".git" or basename == ".hg":
                perr(path, "There is a {0} repository".format(basename), -1)
            else:
                if basename[0] == '.':
                    perr(path + '/' + basename, "Hidden directory", -1)
                checkDir(path + '/' + f, opt)
        else:
            if basename[0] == '.':
                perr(path + '/' + basename, "Hidden file", -1)
            checkFile(path + '/' + f, opt)


def checkFile(path, opt):
    if os.path.isdir(path):
        checkDir(path, opt)
    else:
        baseName = os.path.basename(path)
        if baseName.upper() == "AUTHORS":
            authors.checkFile(path, opt)

        if baseName[len(baseName)-1] == '~' or baseName[len(baseName)-1] == '#'\
               or baseName[0] == '#':
            perr(path, "This is a temporary file", -1)

        iExt = baseName.rfind('.')
        if iExt > 0:
            ext = baseName[iExt:]
            try:
                if ext == ".c" or ext == ".h":
                    moulinette.checkCFile(path)
                    c.checkFile(path, opt)
                elif ext == ".sh":
                    sh.checkFile(path, opt)
                elif ext != ".o" and ext != ".so" and ext != ".a" and ext != ".in":
                    common.checkFile(path, opt)
            except NameError as e:
                print "Hmmm, something fail: {0}. Sorry, you may report this bug.".format(e)
            except IndexError:
                pass

if len(sys.argv) < 2:
    checkFile(os.getcwd(), dict())
else:
    i = 1
    while i < len(sys.argv):
        checkFile(sys.argv[i], dict())
        i += 1
