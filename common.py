#!/usr/bin/python

import re
from report import perr

def checkTrailingWhitespace(path, line, num):
    if len(line) > 1 and (line[len(line)-1] == ' ' or line[len(line)-1] == '\t'):
        perr(path, "Trailing whitespace", num)

def check80cols(path, line, num):
    if len(line) > 79:
        perr(path, "80 columns exceeded", num)


def checkFile(path, opt):
    num = 0
    with open(path, "r") as fp:
        lines = fp.read().split('\n')
        for line in lines:
            num += 1
            line = line.replace("	", "        ")

            checkTrailingWhitespace(path, line, num)
            check80cols(path, line, num)
